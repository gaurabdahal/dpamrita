import socket
import multiprocessing
import sys
from threading import * 
import time
import random
fork=dict()
# Create a TCP/IP socket UDP SERVER
forkaddr=[]
portt=[5006,5006,5006,5006,5006]
#find index of the fork process.
def findIndex(add):
	return forkaddr.index(add)
#method to assign left fork to philosopher using index.
def assignLeftFork(i):

	left=0
	a=i%(len(forkaddr))
	if forkaddr[a]:
		left=a
	else:
		left=len(forkaddr)-1
			
	return forkaddr[left],portt[left]	
#method to assign left fork to philosopher using index.
def assignRightFork(i):	
	right=0	
	b=((i+1)%(len(forkaddr)))        
	if forkaddr[b]:
		right=b
	else:
		right=len(forkaddr)-1
	return forkaddr[right],portt[right]
	
#method to create philosopher process.
def philosopher(address,port=5006):
	print "thinking"	
	time.sleep(random.randint(3, 4))#thinking
        #TCP client 	
	soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	run=True    
	i=findIndex(address)		
	leftFork=assignLeftFork(i)	
	#leftFork=forkaddr[left]			
	rightFork=assignRightFork(i)			
	#rightFork=forkaddr[right]		
			
	while(run):
		soc.connect((address,port))		
		soc.sendto(str(1),leftFork)		
					
		print "waiting",leftFork		
			
		#waiting		
		soc.sendto(str(1),rightFork)		
		data=soc.recv(1024)			
		print(data)		
		if data=="1":
			time.sleep(random.randint(3, 6))#eating		
			print "eating"		
			soc.sendto(str(2), (leftFork))
                        print "releasing left fork"
                        time.sleep(random.randint(3, 6))
			soc.sendto(str(2), (rightFork))	
                        print "Releasing right fork"
                        time.sleep(random.randint(3, 6))
                         	
			run=False		
		elif data=="0":
				
			soc.sendto(str(2), (leftFork))
		        time.sleep(random.randint(3, 6))
                        print "Releasing left fork if right fork is not acquired"		
			#leftFork, rightFork=rightFork,leftFork			
			#socket.close()
			print("hey",port)
#UDP Server
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#fork=['192.168.1.1':0,]
# Bind the socket to the port
ip=raw_input("Enter the ip of monitor: ")

server_address = (ip, 10068)
print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
d=0
a=True
while a:
    print >>sys.stderr, '\nwaiting to receive message'
    data, address = sock.recvfrom(4096)
    print(address,"Received from",data)
    
    if len(forkaddr)<5:
    	forkaddr.append(address[0])
    	fork[d]=0
	d+=1    
	if len(forkaddr)==5:
		a=False		
    	#sock.close()

#forkaddr=['140.158.130.33','140.158.130.33','140.158.130.33','140.158.130.33','140.158.130.33']
print(forkaddr)	


#   print(fork)

#portt=[5070,5071,5072,5073,5074]

#portt=[5340,5341,5342,5343,5344]
for i in range(5):
	print("philosopher #"+str(i)+"  ")	
	philosophers = multiprocessing.Process(target=philosopher,
                                              args=(forkaddr[i],portt[i],))
        #philosophers_processes.append(philosopher)
        philosophers.start()
time.sleep(90)


